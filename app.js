const express = require("express"),
app = express(),
bodyparser = require("body-parser"),
notes = require('./modules/notes');


app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:true}));
app.use('/notes',notes);
app.listen(8080);