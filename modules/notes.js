const express = require("express"),
Router = express.Router(),
bodyparser = require("body-parser"),
mongoose = require('mongoose'),
Schema = mongoose.Schema;


mongoose.connect("mongodb://localhost/test");

const notesSchemaJSON = {
	title: String,
	content: String
};

var notes_schema = new Schema(notesSchemaJSON);
var notes = mongoose.model("notes",notes_schema);

Router.get("/",(req,res)=>{
	notes.find({},(error,doc)=>{
		if(error){
			console.log(error);
			res.status(500).send();
		}
		else{
			res.status(200).send(doc);
		}
	});
});

Router.post("/",(req,res)=>{
	notes = new notes({title:req.body.title, content:req.body.content});
	notes.save((error)=>{
		if(error){
			res.status(500).send('');
		}
		else{
			res.status(200).send('Test created');
		}
	});
});

Router.get('/:id/delete', (req, res, next) => {
	notes.findOneAndRemove({'_id':req.params.id},(error)=>{
		if(error){
			console.log(error);
			res.status(500).send();
		}
		else{
			res.status(200).send('Note deleted');
		}
	});
});

Router.get('/:id/edit', (req, res, next) => {
	notes.findByIdAndUpdate({'_id':req.body.id},{title:req.body.title, content:req.body.content},(error)=>{
		if(error){
			console.log(error);
			res.status(500).send();
		}
		else{
			res.status(200).send('Note updated');
		}
	});
});

module.exports = Router;